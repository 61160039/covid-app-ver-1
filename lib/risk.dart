import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class riskWidget extends StatefulWidget {
  riskWidget({Key? key}) : super(key: key);

  @override
  _riskWidgetState createState() => _riskWidgetState();
}

class _riskWidgetState extends State<riskWidget> {
  var riskValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  var risks = [
    'โรคทางเดินหายใจเรื้อรัง',
    'โรคหัวใจและหลอดเลือด',
    'โรคไตวายเรื้อรัง',
    'โรคหลอดเลือดสมอง',
    'โรคอ้วน',
    'โรคมะเร็ง',
    'โรคเบาหวาน',
  ];

  Widget _lineListTile(int num) {
    return CheckboxListTile(
        value: riskValues[num],
        title: Text(risks[num]),
        onChanged: (newValue) {
          setState(() {
            riskValues[num] = newValue!;
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('risk'),
      ),
      body: ListView(
        children: [
          _lineListTile(0),
          _lineListTile(1),
          _lineListTile(2),
          _lineListTile(3),
          _lineListTile(4),
          _lineListTile(5),
          _lineListTile(6),
          ElevatedButton(
              onPressed: () async {
                await _saverisk();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strriskValue = prefs.getString('risk_values') ??
        "[false, false, false, false, false, false, false]";
    //print(strriskValue.substring(1, strriskValue.length - 1));
    var arrStrriskValues =
        strriskValue.substring(1, strriskValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrriskValues.length; i++) {
        riskValues[i] = (arrStrriskValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saverisk() async {
    var pref = await SharedPreferences.getInstance();
    pref.setString('risk_values', riskValues.toString());
  }
}
